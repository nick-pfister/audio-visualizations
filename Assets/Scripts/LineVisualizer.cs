﻿using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class LineVisualizer : MonoBehaviour
{
    [SerializeField]
    SpectrumAnalyzer spectrumAnalyzer = null;
    [SerializeField]
    float multiplier = 1f;
    [SerializeField]
    float width = 50f;

    LineRenderer lr;
    bool initialized;
    float currentHeight;

    void Start()
    {
        lr = GetComponent<LineRenderer>();
    }

    void Update()
    {
        if (!spectrumAnalyzer.Initialized)
        {
            return;
        }
        if (!initialized || lr.positionCount != spectrumAnalyzer.NumBins)
        {
            Initialize();
        }
        else
        {
            for (int i = 0; i < lr.positionCount; i++)
            {
                try
                {
                    currentHeight = spectrumAnalyzer.GetValueAtBin(i);
                }
                catch (System.ArgumentException e)
                {
                    Debug.LogError(e.Message);
                    break;
                }
                lr.SetPosition(i, new Vector3(lr.GetPosition(i).x, currentHeight * multiplier, 0f));
            }
        }
    }

    void Initialize()
    {
        int count = spectrumAnalyzer.NumBins;
        lr.positionCount = count;
        float step = width / count;

        for (int i = 0; i < count; i++)
        {
            lr.SetPosition(i, new Vector3(i * step, 0f, 0f));
        }

        initialized = true;
    }
}