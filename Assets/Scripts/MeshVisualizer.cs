﻿using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class MeshVisualizer : MonoBehaviour
{
    [SerializeField]
    private SpectrumAnalyzer spectrumAnalyzer = null;
    [SerializeField]
    private float width = 5f;
    private Vector3[] vertices;
    private Mesh mesh;
    private void Start()
    {
        GenerateGrid();
    }

    private void Update()
    {
        UpdateFirstRowVertices();
        ShiftVertices();
    }

    private void UpdateFirstRowVertices()
    {
        for (int i = 0; i < spectrumAnalyzer.NumBins; i++)
        {
            vertices[i].y = spectrumAnalyzer.GetValueAtBin(i);
        }
    }

    private void ShiftVertices()
    {
        int size = spectrumAnalyzer.NumBins;
        for (int z = size - 1; z > 0; z--)
        {
            for (int x = 0; x < size; x++)
            {
                vertices[x + z * size].y = vertices[x + (z - 1) * size].y;
            }
        }
        mesh.vertices = vertices;
        mesh.RecalculateNormals();
    }

    private void GenerateGrid()
    {
        int size = spectrumAnalyzer.NumBins;
        int numVertices = size * size;
        vertices = new Vector3[numVertices];
        Vector2[] uv = new Vector2[vertices.Length];
        GetComponent<MeshFilter>().mesh = mesh = new Mesh();
        mesh.name = name;
        float step = width / spectrumAnalyzer.NumBins;
        for (int i = 0, z = 0; z < size; z++)
        {
            for (int x = 0; x < size; x++, i++)
            {
                vertices[i] = new Vector3(x * step - width / 2, 0, z * step - width / 2);
            }
        }
        int triCount = 6 * (size - 1) * (size - 1);
        int[] tris = new int[triCount];
        int v = 0;
        for (int z = 0; z < size - 1; z++)
        {
            for (int x = 0; x < size - 1; x++)
            {
                v = GenerateQuad(v, tris, x + size * z, size);
            }
        }
        mesh.vertices = vertices;
        mesh.triangles = tris;
        mesh.RecalculateNormals();
    }

    private int GenerateQuad(int v, int[] tris, int x, int xSize)
    {
        tris[v] = x;
        tris[v + 1] = tris[v + 4] = x + xSize;
        tris[v + 2] = tris[v + 3] = x + 1;
        tris[v + 5] = x + xSize + 1;
        return v + 6;
    }
}