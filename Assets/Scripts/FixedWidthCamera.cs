﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class FixedWidthCamera : MonoBehaviour
{
    [SerializeField]
    float cameraWidth = 10;
    [SerializeField]
    bool setBaseAtOrigin = false;

    Camera cam;

    void Start()
    {
        cam = GetComponent<Camera>();
        cam.orthographic = true;
    }

    void Update()
    {
        cam.orthographicSize = cameraWidth / (2f * cam.aspect);
        if (setBaseAtOrigin)
        {
            transform.position = new Vector3(
                transform.position.x,
                cam.orthographicSize,
                transform.position.z
                );
        }
    }
}