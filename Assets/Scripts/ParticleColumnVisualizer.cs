﻿using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleColumnVisualizer : MonoBehaviour
{
    [SerializeField]
    private SpectrumAnalyzer spectrumAnalyzer = null;
    [SerializeField]
    private float width = 10;
    [SerializeField]
    private float emissionPeriod = .1f;
    [SerializeField]
    private float threshold = .1f;
    [SerializeField, Range(1f, 100f)]
    private float speedMultiplier = 10f;
    private ParticleSystem ps;
    private float elapsed;
    private ParticleSystem.EmitParams emitParams = new ParticleSystem.EmitParams();

    private void Start()
    {
        ps = GetComponent<ParticleSystem>();
    }

    private void Update()
    {
        if (!spectrumAnalyzer.Initialized)
        {
            return;
        }
        float step = width / spectrumAnalyzer.NumBins;
        if (elapsed > emissionPeriod)
        {
            elapsed = 0f;
            float val;
            for (int i = 0; i < spectrumAnalyzer.NumBins; i++)
            {
                val = spectrumAnalyzer.GetValueAtBin(i);
                if (val < threshold)
                {
                    continue;
                }

                emitParams.position = new Vector3(step * i, 0, 0);
                emitParams.startSize = val;
                emitParams.velocity = val * Vector3.up * speedMultiplier;
                ps.Emit(emitParams, 1);
            }
        }
        elapsed += Time.deltaTime;
    }
}