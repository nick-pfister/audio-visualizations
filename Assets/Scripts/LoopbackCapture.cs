﻿using UnityEngine;
using CSCore;
using CSCore.SoundIn;
using CSCore.DSP;
using CSCore.Streams;

public static class LoopbackCapture
{
    static float[] fftBuffer;
    static WasapiCapture capture;
    static FftProvider provider;
    static IWaveSource source;
    static IWaveSource finalSource;

    public static FftSize FFTSize { get; set; } = FftSize.Fft4096;
    public static bool Initialized { get; private set; }
    public static float SampleRate
    {
        get
        {
            if (!Initialized)
            {
                Debug.LogError("Capture not initialized - can't get SampleRate.");
                return 0;
            }
            return capture.WaveFormat.SampleRate;
        }
    }
    public static float[] FFTBuffer
    {
        get
        {
            if (!Initialized)
            {
                Debug.LogError("Capture not initialized - can't get FFTBuffer");
                return null;
            }
            return fftBuffer;
        }
    }

    public static void Initialize()
    {
        capture = new WasapiLoopbackCapture();
        capture.Initialize();
        provider = new FftProvider(capture.WaveFormat.Channels, FFTSize);
        fftBuffer = new float[(int)FFTSize];
        source = new SoundInSource(capture);
        var notificationSource = new SingleBlockNotificationStream(source.ToSampleSource());
        notificationSource.SingleBlockRead += SingleBlockRead;
        finalSource = notificationSource.ToWaveSource();
        capture.DataAvailable += CaptureDataAvailable;
        capture.Start();
        Initialized = true;
    }

    static void CaptureDataAvailable(object sender, DataAvailableEventArgs e)
    {
        finalSource.Read(e.Data, e.Offset, e.ByteCount);
    }

    static void SingleBlockRead(object sender, SingleBlockReadEventArgs e)
    {
        provider.Add(e.Left, e.Right);
    }

    public static void Stop()
    {
        capture.Stop();
        capture.Dispose();
        Initialized = false;
    }

    public static void UpdateFFTData()
    {
        if (!Initialized)
        {
            Debug.LogError("Can't get FFT data - loopback not initialized.");
            return;
        }
        if (provider.IsNewDataAvailable)
        {
            try
            {
                provider.GetFftData(fftBuffer);
            }
            catch (System.ArgumentException e)
            {
                Debug.LogWarning($"Caught new ArgumentException getting fftBuffer. \n" +
                    $"This is likely an issue with CSCore and can be ignored if the analyzer " +
                    $"is still running properly.\n" +
                    $"Message: {e.Message}");
            }

        }
    }
}