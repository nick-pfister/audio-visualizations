﻿using CSCore.DSP;
using UnityEngine;

public class SpectrumAnalyzer : MonoBehaviour
{
    [SerializeField]
    private FftSize fftSize = FftSize.Fft4096;
    [SerializeField]
    private int fftFraction = 16;
    [SerializeField, Range(1, 10)]
    private int binsToAverage = 1;
    [SerializeField]
    private bool dualChannel = false;
    [SerializeField, Range(1, 50)]
    private int maxHeightsToAverage = 20;
    [SerializeField]
    private float desiredMaxHeight = 5f;
    [SerializeField]
    private bool scaleByFrequency;
    [SerializeField]
    private int smoothing = 1;

    public int NumBins { get; private set; }
    public bool Initialized { get; private set; }
    public float FrequencyStep { get; private set; }

    private float multiplier;
    private float[] maxHeights;
    private float[] binCoefficients;
    private float[,] bins;
    private int currentBinArrayIndex;
    private int currentHeightIndex;
    private int lastFftFraction;
    private bool maxHeightChanged;

    public float Multiplier
    {
        get
        {
            if (!maxHeightChanged)
            {
                return multiplier;
            }
            else
            {
                float total = 0;
                for (int i = 0; i < maxHeights.Length; i++)
                {
                    total += maxHeights[i];
                }
                float averageMaxHeight = total / maxHeights.Length;
                if (averageMaxHeight == 0f) { return 0f; }
                multiplier = desiredMaxHeight / averageMaxHeight;
                return multiplier;
            }
        }
    }

    public float GetFrequencyAtBin(int index)
    {
        return index * FrequencyStep;
    }

    //Placing the initialization code in OnEnable allows
    //the scripts to be hot-reloaded in the editor.
    private void OnEnable()
    {
        InitializeCapture();
    }

    private void Update()
    {
        if (!LoopbackCapture.Initialized)
        {
            return;
        }
        //Adds support for changing the FFTSize during runtime.
        if (LoopbackCapture.FFTSize != fftSize)
        {
            InitializeCapture();
        }
        //Adds support for changing fraction during runtime.
        if (binsToAverage != bins.GetLength(0) || lastFftFraction != fftFraction)
        {
            InitializeBins();
        }
        LoopbackCapture.UpdateFFTData();
        PopulateBins();
    }

    private void OnApplicationQuit()
    {
        if (LoopbackCapture.Initialized)
        {
            LoopbackCapture.Stop();
        }
    }

    private void PopulateBins()
    {
        float bin;
        float maxHeight = 0;
        for (int i = 0; i < NumBins; i++)
        {
            bin = bins[currentBinArrayIndex, i] = dualChannel ?
                LoopbackCapture.FFTBuffer[i] + LoopbackCapture.FFTBuffer[LoopbackCapture.FFTBuffer.Length - NumBins + i] :
                LoopbackCapture.FFTBuffer[i];
            if (scaleByFrequency)
            {
                bin /= binCoefficients[i];
            }

            if (bin > maxHeight)
            {
                maxHeight = bin;
            }
        }
        if (maxHeights == null || maxHeights.Length != maxHeightsToAverage)
        {
            maxHeights = new float[maxHeightsToAverage];
            currentHeightIndex = 0;
        }
        maxHeights[currentHeightIndex] = maxHeight;
        maxHeightChanged = true;
        currentHeightIndex++;
        if (currentHeightIndex >= maxHeightsToAverage)
        {
            currentHeightIndex = 0;
        }
        currentBinArrayIndex++;
        if (currentBinArrayIndex >= binsToAverage)
        {
            currentBinArrayIndex = 0;
        }
    }

    public float GetValueAtBin(int index)
    {
        if (index < 0 || index >= NumBins)
        {
            throw new System.ArgumentException("Index out of range!");
        }

        float total = 0;
        int length = bins.GetLength(0);
        int binsSmoothed = 0;
        for (int i = -smoothing; i <= smoothing; i++)
        {
            if (index + i < 0 || index + i >= NumBins) { continue; }
            binsSmoothed++;
            for (int j = 0; j < length; j++)
            {
                total += bins[j, index + i];
            }
        }
        total /= length;
        total /= binsSmoothed;

        if (scaleByFrequency)
        {
            total /= binCoefficients[index];
        }

        return total * Multiplier;
    }

    private void InitializeCapture()
    {
        if (LoopbackCapture.Initialized)
        {
            //Stop the capture to avoid multiple captures
            LoopbackCapture.Stop();
        }
        InitializeBins();
        LoopbackCapture.FFTSize = fftSize;
        LoopbackCapture.Initialize();
        FrequencyStep = LoopbackCapture.SampleRate / (int)LoopbackCapture.FFTSize;
        InitializeBinCoefficients();
        Initialized = true;
    }

    private void InitializeBinCoefficients()
    {
        binCoefficients = new float[NumBins];
        for (int i = 0; i < NumBins; i++)
        {
            binCoefficients[i] = 100000f / GetFrequencyAtBin(i);
        }
    }

    private void InitializeBins()
    {
        currentBinArrayIndex = 0;
        fftFraction = Mathf.Clamp(fftFraction, 1, (int)fftSize);
        lastFftFraction = fftFraction;
        NumBins = (int)fftSize / fftFraction;
        bins = new float[binsToAverage, NumBins];
    }
}