﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SceneLoader : MonoBehaviour
{
    [SerializeField]
    int defaultScene = 1;
    [SerializeField]
    KeyCode nextSceneKey = KeyCode.Space;

    bool loading = false;

    void Start()
    {
        InvokeSceneLoad(defaultScene);
    }

    void Update()
    {
        if (Input.GetKeyDown(nextSceneKey))
        {
            NextScene();
        }
    }

    void NextScene()
    {
        int nextScene = SceneManager.GetActiveScene().buildIndex + 1;
        if (nextScene >= SceneManager.sceneCountInBuildSettings)
        {
            nextScene = 1;
        }
        InvokeSceneLoad(nextScene);
    }

    public void InvokeSceneLoad(int buildIndex)
    {
        StartCoroutine(WaitForSceneLoad(buildIndex));
    }

    IEnumerator WaitForSceneLoad(int buildIndex)
    {
        if (loading)
        {
            yield break;
        }
        if (SceneManager.GetSceneByBuildIndex(buildIndex).isLoaded)
        {
            if (SceneManager.GetActiveScene().buildIndex != buildIndex)
            {
                SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(buildIndex));
            }
            yield break;
        }
        loading = true;
        Scene active = SceneManager.GetActiveScene();
        if (active.buildIndex != 0)
        {
            AsyncOperation unload = SceneManager.UnloadSceneAsync(active);
            while (!unload.isDone)
            {
                yield return null;
            }
        }
        AsyncOperation async = SceneManager.LoadSceneAsync(buildIndex, LoadSceneMode.Additive);
        while (!async.isDone)
        {
            yield return null;
        }
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(buildIndex));
        loading = false;
    }
}