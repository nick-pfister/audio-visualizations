﻿using UnityEngine;

[RequireComponent(typeof(Canvas))]
public class MenuCanvas : MonoBehaviour
{
    [SerializeField]
    KeyCode toggleKey = KeyCode.Escape;

    Canvas canvas;

    void Start()
    {
        canvas = GetComponent<Canvas>();
        canvas.enabled = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(toggleKey))
        {
            canvas.enabled = !canvas.enabled;
        }
    }
}